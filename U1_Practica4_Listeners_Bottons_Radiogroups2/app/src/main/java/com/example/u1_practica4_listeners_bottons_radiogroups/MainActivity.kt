package com.example.u1_practica4_listeners_bottons_radiogroups

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RadioButton
import android.widget.Toast
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*


   class MainActivity : AppCompatActivity(), View.OnClickListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        BotonListener.setOnClickListener {
            Toast.makeText(this, "Boton por Listener",  Toast.LENGTH_SHORT).show()
        }
        BotonClase.setOnClickListener(this)



        rgGroup.setOnCheckedChangeListener { group, checkedId ->
        when(group.findViewById<RadioButton>(checkedId)){
            rbBlack ->{
                Toast.makeText(this, "Esta marcado Negro", Toast.LENGTH_SHORT).show()
                root.setBackgroundColor(Color.GREEN)

            }
            rbWhite ->{
                Toast.makeText(this, "Esta marcado Blanco", Toast.LENGTH_SHORT).show()
                root.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
            rbOther ->{
                Toast.makeText(this, "Esta marcado otro", Toast.LENGTH_SHORT).show()
                root.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
            }
           }
        }
    }
    fun onXmlClick(view: View){
            Toast.makeText(this,"Boton por Xml",  Toast.LENGTH_SHORT).show()

        if(rbBlack.isChecked){
             Toast.makeText(this,"Esta marcado", Toast.LENGTH_SHORT).show()
           // rbBlack.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
           // rbWhite.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
           // rbOther.setBackgroundColor(ContextCompat.getColor(this, R.color.white))
             }
    }
        override fun onClick(v: View?) {
            Toast.makeText(this, "Boton por Clase",  Toast.LENGTH_SHORT).show()
        val button = v as Button
        when (button.id){
            R.id.BotonListener -> {
                Toast.makeText(this, "BotonListener",  Toast.LENGTH_SHORT).show()
            }
            R.id.BotonClase -> {
                Toast.makeText(this, "BotonClase",  Toast.LENGTH_SHORT).show()
            }
            R.id.BotonXml -> {
                Toast.makeText(this, "BotonXml",  Toast.LENGTH_SHORT).show()
            }
        }
    }
   }

