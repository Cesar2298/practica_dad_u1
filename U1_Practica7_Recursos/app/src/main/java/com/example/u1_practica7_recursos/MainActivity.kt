package com.example.u1_practica7_recursos

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btnRes.setOnClickListener{
            ivImage.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.pikachu))

        }
        btnUrl.setOnClickListener{
             Picasso.get().load("https://cdn.alfabetajuega.com/wp-content/uploads/2019/07/pokemon-pikachu-electricidad.jpg").into(ivImage)

        }

    }
}