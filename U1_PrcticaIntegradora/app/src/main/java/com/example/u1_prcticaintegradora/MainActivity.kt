package com.example.u1_prcticaintegradora

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        rgGroup.setOnCheckedChangeListener { group, checkedId ->
            when(group.findViewById<RadioButton>(checkedId)){
                btn1 ->{
                    var text = "Americano"
                    var texto = ""
                    var textos= ""
                    val cafe: Int = 20
                    var crema: Int = 0
                    var azucar: Int = 0
                    var cantidad = if (btCantidad.text.isNotEmpty()){
                        btCantidad.text.toString().toInt()
                    }
                    else{
                       1
                    }

                    btnAzucar.setOnCheckedChangeListener { buttonView, isChecked ->
                        if(isChecked){
                            azucar = 1
                            var texto = "Azucar"
                        }
                        else{
                            azucar = 0
                        }
                    }
                    btnCrema.setOnCheckedChangeListener { buttonView, isChecked ->
                        if (isChecked){
                            crema = 1
                            var textos = "Crema"
                        }
                        else {
                            crema= 0
                        }
                    }
                    btnTotal.setOnClickListener {
                        var resultado = cantidad*cafe
                        var resultado2= resultado+crema+azucar
                        Toast.makeText(this, "El total a pagar es: $"+resultado2,Toast.LENGTH_SHORT).show()
                        txt.text = text+""+texto+""+textos
                    }
                }
                btn2 ->{
                    var text = "Capuchino"
                    var texto = ""
                    var textos= ""
                    var cafe: Int = 48
                    var crema: Int = 0
                    var azucar: Int = 0
                    var cantidad = if (btCantidad.text.isNotEmpty()){
                        btCantidad.text.toString().toInt()
                    }
                    else{
                        1
                    }

                    btnAzucar.setOnCheckedChangeListener { buttonView, isChecked ->
                        if(isChecked){
                            azucar = 1
                            var texto = "Azucar"
                        }
                        else{
                            azucar = 0
                        }
                    }
                    btnCrema.setOnCheckedChangeListener { buttonView, isChecked ->
                        if (isChecked){
                            crema = 1
                            var textos = "Crema"
                        }
                        else {
                            crema= 0
                        }
                    }
                    btnTotal.setOnClickListener {
                        var resultado = cantidad*cafe
                        var resultado2= resultado+crema+azucar
                        Toast.makeText(this, "El total a pagar es: $"+resultado2,Toast.LENGTH_SHORT).show()
                        txt.text = text+""+texto+""+textos
                    }
                }
                btn3 ->{
                    var text = "Expreso"
                    var texto = ""
                    var textos= ""
                    var cafe: Int = 30
                    var crema: Int = 0
                    var azucar: Int = 0
                    var cantidad = if (btCantidad.text.isNotEmpty()){
                        btCantidad.text.toString().toInt()
                    }
                    else{
                        1
                    }
                    btnAzucar.setOnCheckedChangeListener { buttonView, isChecked ->
                        if(isChecked){
                            azucar = 1
                            var texto = "Azucar"
                        }
                        else{
                            azucar = 0
                        }
                    }
                    btnCrema.setOnCheckedChangeListener { buttonView, isChecked ->
                        if (isChecked){
                            crema = 1
                            var textos = "Crema"
                        }
                        else {
                            crema= 0
                        }
                    }
                    btnTotal.setOnClickListener {
                        var resultado = cantidad*cafe
                        var resultado2= resultado+crema+azucar
                        Toast.makeText(this, "El total a pagar es: " + resultado2 ,Toast.LENGTH_SHORT).show()
                        txt.text = text+""+texto+""+textos

                    }
                }

            }
        }

    }
}
