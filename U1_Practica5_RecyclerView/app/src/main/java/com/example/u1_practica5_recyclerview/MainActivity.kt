package com.example.u1_practica5_recyclerview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    private val adapter by lazy {
        CustomAdapter { person, pos ->
            Toast.makeText(this, "Persona $pos: ${person.name}, ${person.age} ", Toast.LENGTH_SHORT).show()

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*val adapter =CustomAdapter()*/
        rvlist.adapter = adapter

        val list = mutableListOf<Person>()
        for (i in 0..7) {
            list.add(Person("Lalo $i", "Perez $i", 15 + i))
        }

        btnAdd.setOnClickListener {
            val newPerson = Person(etName.text.toString(), etLastname.text.toString(), etAge.text.toString().toInt())

            list.add(newPerson)
            //adapter.setList(list)
            adapter.addPerson(newPerson)
        }

        adapter.setList(list)

    }
}