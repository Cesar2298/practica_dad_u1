package com.example.u1_practica3_layouts_y_componentes

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        BotonInfo.setOnClickListener {
            if (etNombre.text.isNotEmpty() && etApellidos.text.isNotEmpty() && etEdad.text.isNotEmpty()){
                val persona = when {
                    rbHombre.isChecked -> {
                        val persona = Persona(etNombre.text.toString(), etApellidos.text.toString(), etEdad.text.toString.toInt(), "Hombre")
                    }
                    rbMujer.isChecked -> {
                        val persona = Persona(etNombre.text.toString(), etApellidos.text.toString(), etEdad.text.toString.toInt(), "Mujer")
                    }
                    else -> {
                        val persona = Persona(etNombre.text.toString(), etApellidos.text.toString(), etEdad.text.toString.toInt(), "No Especifica")
                    }
                }

                Toast.makeText(this, "Persona de Nombre: ${persona.Nombre} ${persona.Apellidos}\n" +
                        "Edad: ${persona.Edad}\n De Género: ${persona.Género}", Toast.LENGTH_LONG).show()


            }else{
                Toast.makeText(this, "Por favor rellena la información solicitada", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

data class Persona(var Nombre: String, var Apellidos: String, var Edad:Int, var Género: String)