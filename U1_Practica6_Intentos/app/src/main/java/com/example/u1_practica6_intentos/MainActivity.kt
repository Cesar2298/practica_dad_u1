package com.example.u1_practica6_intentos

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

const val TEXT_EXTRA = "text_extra"

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnGo.setOnClickListener {
            val bundle = Bundle()
            val intent = Intent(this, OtherActivity::class.java)
            intent.putExtra(TEXT_EXTRA, etTexto.text.toString())
            //bundle.putString(TEXT_EXTRA, etTexto.text.toString())
            startActivity(intent)
                                 //bundle
        }
    }
}